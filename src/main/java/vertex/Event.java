package vertex;


import com.fasterxml.jackson.databind.introspect.TypeResolutionContext;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import lombok.Builder;
import util.Generator;

import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Builder
public class Event {
    Long eventId;
    @Builder.Default
    String title = "Title of the event";

    @Builder.Default
    String description = "Description of the event";
    @Builder.Default
    List<String> links = List.of("l1","l2","l3");

    LocalDateTime start;
    LocalDateTime end;

    String initiator; //RID

    public String[] toVertexArgs(){
        String[] res = new String[14];
        res [0] = "eventId";
        res [1] = eventId.toString();
        res [2] = "title";
        res [3] = title;
        res [4] = "description";
        res [5] = description;
        res [6] = "links";
        res [7] = String.join(",", links);
        res [8] = "start";
        res [9] = start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        res [10] = "end";
        res [11] = end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        res [12] = "initiator";
        res [13] = initiator;
        return res;
    }


}
