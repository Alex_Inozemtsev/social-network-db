package vertex;

import lombok.Builder;
import util.FileParser;
import util.Generator;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Builder
public class User {

    private String username;

    private String firstName;
    private String lastName;

    @Builder.Default
    private String password = "Encrypted_Password";
    @Builder.Default
    private String profileDescription = "Description";
    @Builder.Default
    private String email = "example@post.com";
    private LocalDateTime lastLogin ;
    private LocalDateTime accountCreated;
    @Builder.Default
    private String profileImage = "/images/photo.img";

    private String country;
    private String city;

    private Boolean isAccountPrivate;

    private LocalDateTime accountDeletionTime;


    public String[] toVertexArgs(){
        String[] res = new String[24];
        res [0] = "username";
        res [1] = username;
        res [2] = "firstName";
        res [3] = firstName;
        res [4] = "lastName";
        res [5] = lastName;
        res [6] = "profileDescription";
        res [7] = profileDescription;
        res [8] = "email";
        res [9] = email;
        res [10] = "lastLogin";
        res [11] = lastLogin.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        res [12] = "accountCreated";
        res [13] = accountCreated.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        res [14] = "profileImage";
        res [15] = profileImage;
        res [16] = "country";
        res [17] = country;
        res [18] = "city";
        res [19] = city;
        res [20] = "isAccountPrivate";
        res [21] = isAccountPrivate.toString();
        res [22] = "password";
        res [23] = password;
        return res;
    }



    public static List<User> genUsers(){
        String usernamesPath = "/home/alexinozemtsev/mephi/6 term/oait/usernames.txt";
        String countryPath = "/home/alexinozemtsev/mephi/6 term/oait/country.txt";
        String cityPath = "/home/alexinozemtsev/mephi/6 term/oait/city.txt";
        String firstnamePath = "/home/alexinozemtsev/mephi/6 term/oait/firstnames.txt";
        String lastnamePath = "/home/alexinozemtsev/mephi/6 term/oait/lastnames.txt";

        List<String> usernames = FileParser.getItems(usernamesPath);
        List<String> cities = FileParser.getItems(cityPath);
        List<String> countries = FileParser.getItems(countryPath);
        List<String> firstnames = FileParser.getItems(firstnamePath);
        List<String> lastnames = FileParser.getItems(lastnamePath);

        Timestamp createBegin = Timestamp.valueOf("2010-01-01 00:00:00");
        Timestamp createEnd = Timestamp.valueOf("2022-01-01 00:00:00");
        Timestamp loginBegin = Timestamp.valueOf("2022-01-01 00:00:00");
        Timestamp loginEnd = Timestamp.valueOf("2022-05-10 00:00:00");



        List<User> users = new LinkedList<>();

        Random rand = new Random();

        for (String username : usernames){
            int countryCityIndex = rand.nextInt(countries.size());
            users.add(
                    User.builder()
                            .username(username)
                            .firstName(
                                    firstnames.get(rand.nextInt(firstnames.size()))
                            )
                            .lastName(
                                    lastnames.get(rand.nextInt(firstnames.size()))
                            )
                            .city(
                                    cities.get(countryCityIndex)
                            )
                            .country(
                                    countries.get(countryCityIndex)
                            )
                            .lastLogin(
                                    Generator.randomDateTimeBetween(
                                            loginBegin,loginEnd
                                    )
                            )
                            .accountCreated(
                                    Generator.randomDateTimeBetween(
                                            createBegin,createEnd
                                    )
                            )
                            .isAccountPrivate(rand.nextBoolean())
                            .build()
            );
        }

        return users;
    }
}
