package vertex;

import lombok.Builder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Builder
public class Tag {
    LocalDateTime firstUsage;
    String name;

    public String[] toVertexArgs(){
        String[] res = new String[4];
        res [0] = "firstUsage";
        res [1] = firstUsage.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        res [2] = "name";
        res [3] = name;
        return res;
    }
}
