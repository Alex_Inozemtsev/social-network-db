package vertex;


import lombok.Builder;

import java.time.format.DateTimeFormatter;

@Builder
public class Comment {
    Long commentId;
    String content;

    public String[] toVertexArgs(){
        String[] res = new String[4];
        res [0] = "commentId";
        res [1] = commentId.toString();
        res [2] = "content";
        res [3] = content;
        return res;
    }
}
