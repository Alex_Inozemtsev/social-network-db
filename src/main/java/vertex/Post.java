package vertex;

import lombok.AccessLevel;
import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@Builder
public class Post {
    Long postId;
    @Builder.Default
    String textContent = "Text Content";
    @Builder.Default
    String accessLevel = "ALL";
    @Builder.Default
    Set<String> attachments = Set.of(
            "/attachments/attachmentID1",
            "/attachments/attachmentID2"
    );
    LocalDateTime postedAt;

    public String[] toVertexArgs(){
        String[] res = new String[10];
        res [0] = "postId";
        res [1] = postId.toString();
        res [2] = "textContent";
        res [3] = textContent;
        res [4] = "attachments";
        res [5] = String.join(",", attachments);;
        res [6] = "postedAt";
        res [7] = postedAt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        res [8] = "accessLevel";
        res [9] = accessLevel;

        return res;
    }
}
