import com.fasterxml.jackson.core.JsonProcessingException;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;


public class Main {
    public static void main(String[] args) throws JsonProcessingException {

        OrientGraph graph = new OrientGraph("remote:localhost/SNS", "admin", "admin");

        /*
        Use static methods from DataSaver here
        */

        graph.shutdown();

    }
}
