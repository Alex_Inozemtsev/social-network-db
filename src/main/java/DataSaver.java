import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientDynaElementIterable;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import util.FileParser;
import util.Generator;
import vertex.*;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class DataSaver {

    private static long getId(String sequence, OrientGraph graph){
        OrientDynaElementIterable res =
                graph.command(new OCommandSQL("SELECT sequence('"+sequence+"').next()"))
                .execute();
        OrientVertex v  = (OrientVertex) res.iterator().next();
        return v.getProperty("sequence");
    }


    public static void saveTags(OrientGraph graph){
        String tagsPath = "/home/alexinozemtsev/mephi/6 term/oait/tags.txt";

        List<String> tagNames = FileParser.getItems(tagsPath);

        Timestamp begin = Timestamp.valueOf("2015-01-01 00:00:00");
        Timestamp end = Timestamp.valueOf("2022-05-10 00:00:00");

        for(String tagName: tagNames){
            Tag tag = Tag.builder()
                    .name(tagName)
                    .firstUsage(Generator.randomDateTimeBetween(begin,end)).build();
            graph.addVertex("class:Tag", tag.toVertexArgs());
        }

    }
    public static void savePosts(OrientGraph graph, int numOfPosts){
        Timestamp begin = Timestamp.valueOf("2015-01-01 00:00:00");
        Timestamp end = Timestamp.valueOf("2022-05-10 00:00:00");


        for (int i = 0; i<numOfPosts; i++){
            Post post = Post.builder()
                    .postId(getId("postId", graph))
                    .postedAt(Generator.randomDateTimeBetween(begin, end))
                    .build();
            graph.addVertex("class:Post", post.toVertexArgs());
        }
    }

    public static void saveEvents(OrientGraph graph, int numOfEvents){
        Random rand = new Random();

        List<String> rids = new ArrayList<>();
        for (Object v : (OrientDynaElementIterable) graph.command(
                new OCommandSQL("SELECT * from User where @rid like \"#34:%\"")
        ).execute()) {
            rids.add(
                    ((OrientVertex) v).getIdentity().getClusterId() + ":" +
                            ((OrientVertex) v).getIdentity().getClusterPosition()
            );
        }

        for(int i = 0; i < numOfEvents; i++){
            long id = getId("eventId", graph);
            int cluster = graph.getVertexType("User").getClusterIds()[0];


            Timestamp begin = Timestamp.valueOf("2022-01-01 00:00:00");
            Timestamp end = Timestamp.valueOf("2023-05-10 00:00:00");


            Event toSave = Event.builder()
                    .eventId(id)
                    .initiator(rids.get(rand.nextInt(rids.size()-1)))
                    .start(Generator.randomDateTimeBetween(begin,end))
                    .end(Generator.randomDateTimeBetween(begin,end)).build();
            graph.addVertex("class:Event", toSave.toVertexArgs());
        }
    }

    public static void saveUsers(OrientGraph graph){
        List<User> userList = User.genUsers();

        for (User u : userList){
            graph.addVertex("class:User", u.toVertexArgs());
        }
    }

    public static void saveFriendEdge(OrientGraph graph) {
        Random rand = new Random();
        ArrayList<Vertex> vs =  new ArrayList<Vertex>();
        graph.getVerticesOfClass("vertex.User").iterator().forEachRemaining(vs::add);

        for (int i = 0; i < 100000; i++) {
            int i1 = rand.nextInt(vs.size());
            int i2 = rand.nextInt(vs.size());
            while (i2 == i1)
                i2 = rand.nextInt(vs.size());

            Vertex v1 = vs.get(i1);
            Vertex v2 = vs.get(i2);

            graph.addEdge(null, v1, v2, "Friend");
            graph.addEdge(null, v2, v1, "Friend");
        }
    }

    public static void saveComments(OrientGraph graph, int numOfComments){

        for (int  i = 0; i<numOfComments;i++) {
            graph.addVertex(
                    "class:Comment",
                    Comment.builder()
                            .commentId(getId("commentId",graph))
                            .content("Comment text").build().toVertexArgs()
            );
        }


    }
    public static void saveAuthorshipEdge(OrientGraph graph){
        Random rand = new Random();
        ArrayList<Vertex> users =  new ArrayList<Vertex>();
        ArrayList<Vertex> posts =  new ArrayList<Vertex>();
        ArrayList<Vertex> comments =  new ArrayList<Vertex>();

        graph.getVerticesOfClass("User").iterator()
                .forEachRemaining(users::add);

        graph.getVerticesOfClass("Post").iterator()
                .forEachRemaining(posts::add);

        graph.getVerticesOfClass("Comment").iterator()
                .forEachRemaining(comments::add);

        for (Vertex post: posts) {
            int i1 = rand.nextInt(users.size());

            Vertex user = users.get(i1);

            graph.addEdge(null, user, post, "Authorship");
            graph.addEdge(null, post, user, "Authorship");
        }

        /*for (Vertex comment: comments) {
            int i1 = rand.nextInt(users.size());

            Vertex user = users.get(i1);

            graph.addEdge(null, user, comment, "Authorship");
            graph.addEdge(null, comment, user, "Authorship");
        }*/
    }

    public static void saveSubscriberEdge(OrientGraph graph) {
        Random rand = new Random();
        ArrayList<Vertex> vs =  new ArrayList<Vertex>();
        graph.getVerticesOfClass("User").iterator().forEachRemaining(vs::add);

        for (int i = 0; i < 10000000; i++) {
            int i1 = rand.nextInt(vs.size()-1);
            int i2 = rand.nextInt(vs.size()-1);
            while (i2 == i1)
                i2 = rand.nextInt(vs.size()-1);

            Vertex v1 = vs.get(i1);
            Vertex v2 = vs.get(i2);

            graph.addEdge(null, v1, v2, "Subscriber");
        }
    }

    public static void saveFriendRequestEdge(OrientGraph graph) {
        Random rand = new Random();
        ArrayList<Vertex> vs =  new ArrayList<Vertex>();
        graph.getVerticesOfClass("User").iterator().forEachRemaining(vs::add);

        for (int i = 0; i < 1000; i++) {
            int i1 = rand.nextInt(vs.size()-1);
            int i2 = rand.nextInt(vs.size()-1);
            while (i2 == i1)
                i2 = rand.nextInt(vs.size()-1);

            Vertex v1 = vs.get(i1);
            Vertex v2 = vs.get(i2);

            graph.addEdge(null, v1, v2, "FriendReqest");
        }
    }

    public static void saveReactionEdge(OrientGraph graph){
        String[] types = {"LIKE","DISLIKE", "LOVE", "LAUGH"};

        Timestamp begin = Timestamp.valueOf("2021-01-01 00:00:00");
        Timestamp end = Timestamp.valueOf("2022-05-10 00:00:00");

        Random rand = new Random();
        ArrayList<Vertex> vs =  new ArrayList<Vertex>();
        ArrayList<Vertex> posts =  new ArrayList<Vertex>();
        ArrayList<Vertex> comments =  new ArrayList<Vertex>();

        graph.getVerticesOfClass("User").iterator()
                .forEachRemaining(vs::add);
        graph.getVerticesOfClass("Post").iterator()
                .forEachRemaining(posts::add);

        System.out.println("Reading done");
        for (int i = 0; i < 100000; i++) {
            int i1 = rand.nextInt(vs.size());
            int i2 = rand.nextInt(posts.size());

            Vertex v1 = vs.get(i1);
            Vertex v2 = posts.get(i2);

            graph.addEdge(null, v1, v2, "Reaction")
                    .setProperties("reactionType", types[rand.nextInt(types.length)],
                            "reactionTime", Generator.randomDateTimeBetween(begin,end).format(
                                    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                            ));

        }

    }
}
