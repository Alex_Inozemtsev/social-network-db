package util;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class Generator {
    public static LocalDateTime randomDateTimeBetween(
            Timestamp t1, Timestamp t2
    ){
        long diff = t2.getTime() - t1.getTime() + 1;
        long temp =  t1.getTime() + (long) (Math.random() * diff);
        return LocalDateTime.ofInstant(
                Instant.ofEpochMilli(temp), TimeZone.getDefault().toZoneId());
    }

}
