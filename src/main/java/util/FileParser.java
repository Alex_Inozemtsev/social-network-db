package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class FileParser {

    public static List<String> getItems (String filename) {
        List<List<String>> items = new LinkedList<>();

        try (Scanner scanner = new Scanner(new File(filename));) {
            while (scanner.hasNextLine()) {
               items.add(getItemsFromLine(scanner.nextLine()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return items.stream().flatMap(Collection::stream).toList();
    }

    private static List<String> getItemsFromLine(String line) {
        List<String> items = new LinkedList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            while (rowScanner.hasNext()) {
                items.add(rowScanner.next());
            }
        }
        return items;
    }

}
